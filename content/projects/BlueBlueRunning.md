---
title: "BlueBlueRunning"
date: 2014-07-16
#repo: "#" # delete this line if you want blog-like posts for projects
#tags: ["android", "app", "running", "google services", "maps"]
weight: 1
draft: false
---

**Authors**: Barbieri Simone, Loddo Andrea, Mameli Emanuele, Muntoni Alessandro, Pompianu Livio

**University project:** Final Project for the unit *Operating System 2* for the Master Degree in Computer Science at the University of Cagliari.

A five people project which consists in an Android application which traks user's running trainings and it allows the users to save locally (in .GPX format) and share them with Google services such as Google Spreadsheet, Google Drive, Google Maps and Google Earth.

