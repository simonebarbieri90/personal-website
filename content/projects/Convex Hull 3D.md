---
title: "Convex Hull 3D"
date: 2013-09-06
#repo: "#" # delete this line if you want blog-like posts for projects
#tags: ["convex-hull", "3d meshes", "geometry"]
weight: 1
draft: false
---

![](/resources/portfolio-img/convex-hull.png)

**University project:** Final Project for the unit *Algorithms and Data Structures 2* for the Master Degree in Computer Science at the University of Cagliari.

It implements a Convex Hull of a set of points in a 3D space.

