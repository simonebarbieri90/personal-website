---
title: "Voronoi Clarke & Wright Algorithm"
date: 2014-06-18
#repo: "#" # delete this line if you want blog-like posts for projects
#tags: ["voronoi", "clarke-wright", "programming", "operative research"]
weight: 1
draft: false
---

![](/resources/portfolio-img/voronoi-clarke-wright.png)

**Authors**: Barbieri Simone, Loddo Andrea, Mameli Emanuele, Muntoni Alessandro, Pompianu Livio

**University project:** Final Project for the unit *Operative Research* for the Master Degree in Computer Science at the University of Cagliari.

A five people project which implements a modified version of Clarke and Wright Algorithm, for the vehicle routing problem, using the Voronoi tasselation.