---
title: "About Me"
description: " "
date: 2018-02-12
#tags: ["go", "golang", "hugo", "themes"]
draft: false
---

![image alt <](/resources/profile-pic.jpg)

I am a Research Engineer (EngD) at the Centre of Digital Entertainment, 
Bournemouth University. My research is funded by the EPSRC and I am 
currently looking for industrial collaborators.					

In 2012, I obtained my BSc and, in 2014, my MSc, both in Computer Science at University of Cagliari, in Italy. 

In my work, I am dealing with skeletons, rigging and computer 
animation. I am interested in applying a sketch-based user interface in 
computer graphics tasks. Therefore, my current research focuses on the 
character posing and modelling through hand-drawn sketches.

At the moment, I am working on a technique that will allow the 
users to draw a few simple sketches which will not only pose the 
character but also guide the detailed deformation on the shape flow, 
allowing him to draw just a partial outline of the character's 
components, leaving untouched the other ones.
​				