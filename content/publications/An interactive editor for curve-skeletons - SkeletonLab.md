---
title: "An interactive editor for curve-skeletons: SkeletonLab"
#description: " "
date: 2016-08-26
#repo: "#" # delete this line if you want blog-like posts for projects
#tags: ["curve-skeleton", "3d meshes", "geometry processing", "interactive editing"]
weight: 1
draft: false
---

![](/resources/portfolio-img/skel-lab2.jpg)

<br>

#### **Authors**

[Barbieri Simone](http://barbierisimone.com/), Centre for Digital Entertainment, Bournemouth University

Meloni Pietro, Universitá degli studi di Cagliari

[Usai Francesco](http://francescousai.info/), Universitá degli studi di Cagliari

[Spano Davide Lucio](http://people.unica.it/davidespano/), Universitá degli studi di Cagliari

[Scateni Riccardo](http://people.unica.it/riccardoscateni/), Universitá degli studi di Cagliari

<br>

#### **Abstract**

Curve-skeletons are powerful shape descriptors able to provide higher level information on topology, structure and semantics of a given digital object. Their range of application is wide and encompasses computer animation, shape matching, modelling and remeshing. While a universally accepted definition of curve-skeleton is still lacking, there are currently many algorithms for the curve-skeleton computation (or skeletonization) as well as different techniques for building a mesh around a given curve-skeleton (inverse skeletonization). Despite their widespread use, automatically extracted skeletons usually need to be processed in order to be used in further stages of any pipeline, due to different requirements. We present here an advanced tool, named SkeletonLab, that provides simple interactive techniques to rapidly and automatically edit and repair curve skeletons generated using different techniques proposed in the literature, as well as handcrafting them. The aim of the tool is to allow trained practitioners to manipulate the curve-skeletons obtained with skeletonization algorithms in order to fit their specific pipelines or to explore the requirements of newly developed techniques.

<br>

#### **Paper**

![](/resources/pdf-icon.png)[An interactive editor for curve-skeletons: SkeletonLab](/downloads/An%20interactive%20editor%20for%20curve-skeletons%20-%20SkeletonLab.pdf) (6.90 MB)

Published in [Computers & Graphics](http://www.journals.elsevier.com/computers-and-graphics)

**DOI**: <https://doi.org/10.1016/j.cag.2016.08.002>

<br>

#### **Bibtex**

``` latex
@article{BARBIERI201623,
    title = "An interactive editor for curve-skeletons: SkeletonLab",
    journal = "Computers & Graphics",
    volume = "60",
    pages = "23 - 33",
    year = "2016",
    issn = "0097-8493",
    doi = "https://doi.org/10.1016/j.cag.2016.08.002",
    url = "http://www.sciencedirect.com/science/article/pii/S0097849316300905",
    author = "Simone Barbieri and Pietro Meloni and Francesco Usai and L. Davide Spano and Riccardo Scateni",
    keywords = "Curve-skeleton, 3D meshes, Geometry processing, Interactive editing"
}
```