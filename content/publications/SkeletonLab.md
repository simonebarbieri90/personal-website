---
title: "Skeleton Lab: an Interactive Tool to Create, Edit, and Repair Curve-Skeletons"
date: 2015-10-15
#repo: "#" # delete this line if you want blog-like posts for projects
#tags: ["curve-skeleton", "3d meshes", "geometry processing", "interactive editing"]
weight: 1
draft: false
---

![](/resources/portfolio-img/dragon-compose.png)

<br>

#### **Authors**

[Barbieri Simone](http://barbierisimone.com/), Universitá degli studi di Cagliari

Meloni Pietro, Universitá degli studi di Cagliari

[Usai Francesco](http://francescousai.info/), Universitá degli studi di Cagliari

[Scateni Riccardo](http://people.unica.it/riccardoscateni/), Universitá degli studi di Cagliari

<br>

#### **Abstract**

Curve-skeletons are well known shape descriptors, able to encode topological and structural information of a shape. The range of applications in which they are used comprises, to name a few, computer animation, shape matching, modelling and remeshing. Different tools for automatically extracting the curve-skeleton for a given input mesh are currently available, as well as inverse skeletonization tools, where a user-defined skeleton is taken as input in order to build a mesh that reflects the encoded structure. Although their use is broad, an automatically extracted curve-skeleton is usually not well-suited for the next pipeline step in which they will be used. We present a tool for creating, editing and repairing curve-skeletons whose aim is to allow users to obtain, within minutes, curve-skeletons that are tailored for their specific task.

<br>

#### **Paper**

![](/resources/pdf-icon.png)[Skeleton Lab: an Interactive Tool to Create, Edit, and Repair Curve-Skeletons](/downloads/Barbieri2015SkelLab.pdf) (12.0 MB)

**Best paper award** at [STAG 2015](http://stag2015.eu/)

**DOI**: <http://dx.doi.org/10.2312/stag.20151299>

<br>

#### **Bibtex**

```latex
@inproceedings {stag.20151299,
    booktitle = {Smart Tools and Apps for Graphics - Eurographics Italian Chapter Conference},
    editor = {Andrea Giachetti and Silvia Biasotti and Marco Tarini},
    title = {{Skeleton Lab: an Interactive Tool to Create, Edit, and Repair Curve-Skeletons}},
    author = {Barbieri, Simone and Meloni, Pietro and Usai, Francesco and Scateni, Riccardo},
    year = {2015},
    publisher = {The Eurographics Association},
    ISBN = {978-3-905674-97-2},
    DOI = {10.2312/stag.20151299}
}
```