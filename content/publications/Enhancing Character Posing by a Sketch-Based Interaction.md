---
title: "Enhancing character posing by a sketch-based interaction"
date: 2016-07-24
#repo: "#" # delete this line if you want blog-like posts for projects
#tags: ["sketches", "curve-skeleton", "outline", "3D meshes", "animation"]
weight: 1
draft: false
---

![](/resources/portfolio-img/enhancing-character-posing.jpg)

<br>

#### **Authors**

[Barbieri Simone](http://barbierisimone.com/), Centre for Digital Entertainment, Bournemouth University

Garau Nicola, Universitá degli studi di Cagliari

Hu Wenyu, Gannan Normal University

[Xiao Zhidong](https://staffprofiles.bournemouth.ac.uk/display/zxiao), Bournemouth University

[Yang Xiaosong](https://staffprofiles.bournemouth.ac.uk/display/xyang), Bournemouth University

<br>

#### **Abstract**

Sketch as the most intuitive and powerful 2D design method has been used by artists for decades. However it is not fully integrated into current 3D animation pipeline as the difficulties of interpreting 2D line drawing into 3D. Several successful research for character posing from sketch has been presented in the past few years, such as the Line of Action [Guay et al. 2013] and Sketch Abstractions [Hahn et al. 2015]. However both of the methods require animators to manually give some initial setup to solve the corresponding problems. In this paper, we propose a new sketch based character posing system which is more flexible and efficient. It requires less input from the user than the system from [Hahn et al. 2015]. The character can be easily posed no matter the sketch represents a skeleton structure or shape contours.

<br>

#### **Paper**

![](/resources/pdf-icon.png)[Enhancing character posing by a sketch-based interaction](/downloads/Enhancing%20Character%20Posing%20by%20a%20Sketch-Based%20Interaction.pdf) (4.41 MB)

Published in [SIGGRAPH 2016 Posters](http://s2016.siggraph.org/)

**DOI**: https://doi.org/10.1145/2945078.2945134

<br>

#### **Downloads**

![](/resources/pdf-icon.png)[SIGGRAPH Poster](/downloads/Enhancing%20Character%20Posing%20by%20a%20Sketch-Based%20Interaction%20-%20Poster.pdf) (6.58 MB)

<br>

#### **Bibtex**

```latex
@inproceedings{Barbieri:2016:ECP:2945078.2945134,
    author = {Barbieri, Simone and Garau, Nicola and Hu, Wenyu and Xiao, Zhidong and Yang, Xiaosong},
    title = {Enhancing Character Posing by a Sketch-based Interaction},
    booktitle = {ACM SIGGRAPH 2016 Posters},
    series = {SIGGRAPH '16},
    year = {2016},
    isbn = {978-1-4503-4371-8},
    location = {Anaheim, California},
    pages = {56:1--56:2},
    articleno = {56},
    numpages = {2},
    url = {http://doi.acm.org/10.1145/2945078.2945134},
    doi = {10.1145/2945078.2945134},
    acmid = {2945134},
    publisher = {ACM},
    address = {New York, NY, USA},
    keywords = {sketch-based posing},
}
```



