---
title: "Curriculum Vitae"
description: " "
date: 2018-02-12
#tags: ["go", "golang", "hugo", "themes"]
draft: false
---

[comment]: &lt;> "Click [here](/downloads/Barbieri%20Simone's%20Curriculum.pdf) to download a PDF copy of the curriculum."

## Work Experience

<br>

#### Current - December 2016

Research Engineer at **Thud Media**, Cardiff

---

#### Current - September 2015

EngD Student at **Bournemouth University**, Bournemouth

**Supervisors**: Dr. Xiasong Yang, Dr. Zhidong Xiao

---

#### December 2014 - March 2015

Research Fellow at **University of Cagliari**, Cagliari

**Advisor**: Prof. Maurizio Atzori

<br><br>

## Education

<br>

#### September 2014

Master Degree in Computer Science, **University of Cagliari**, Cagliari

**Score**: 110/110

**Thesis**: *Skeleton Editing and Mesh Reconstruction from Skeleton*

**Brief Description**: the thesis consists of a skeleton editor, capable of a number of useful feature which allow a simple editing for all users, and an inverse skeletonization algorithm which generate a three-dimensional quad-mesh from the created skeleton.

**Advisor**: Prof. Riccardo Scateni

**Download**: [PDF](/downloads/Master%20Degree%20Thesis%20-%20Simone%20Barbieri.pdf)

---

#### July 2012

Bachelor Degree in Computer Science, **University of Cagliari**, Cagliari

**Score**: 110/110

**Thesis**: *Visual Engine for Reading On Network In Comprehensive Acceptation*

**Brief Description**: V.E.R.O.N.I.C.A. system is a five people project which has as its primary goal the creation of automated support for reading for people suffering from the disorder of dyslexia, through a web-based application accessible from any platform.

**Advisors**: Prof. Massimo Bartoletti, Prof.ssa Silvia Corso, Prof. Gianni Fenu, Prof.ssa Barbara Pes, Prof. Riccardo Scateni

**Download**: [PDF (Italian)](/downloads/Bachelor%20Degree%20Thesis%20-%20Simone%20Barbieri.pdf)

<br><br>

## Languages

<br>

#### Italian

Mothertongue

---

#### English

IELTS level 6.5 (5.5 Speaking - 5.0 Writing - 7.0 Listening - 7.5 Reading)

<br><br>

## Computer Skills

<br>

#### Basic Knowledge

LATEX, OCaml, Assembly, Wordpress, Git, JQuery

---

#### Intermediate Knowledge

Linux, HTML, CSS, PHP, Android SDK, OpenGL, Javascript, PostgreSQL, MySQL

---

#### Advanced Knowledge

C, C++, Qt, Java

<br><br>

## Skills

<br>

- Experience in group working. Most of the university project I have worked on have been carried out in groups of five people.<br>

- Experience in presenting projects carried out in Power Point presentations with different types of audiences.
- Expertise in project planning, including the ability in creating documents such as Requirements Analysis and Project Plan.
- Ability in coordinating small workgroups.

<br><br>

## Interests and Activities

<br>

Technology, Open-Source, Programming, Comics, Video games, Movies, TV Shows, Fencing (**6 years**), Basketball (**2 years**), Swimming (**5 years**), Travelling