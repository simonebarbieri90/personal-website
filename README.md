# Personal Website
The code of my personal website. The website is build with [Hugo](https://gohugo.io/) and with the [Minimal](https://themes.gohugo.io/minimal/) theme.

To install and use Hugo, please refer to the [official documentation](https://gohugo.io/getting-started/).