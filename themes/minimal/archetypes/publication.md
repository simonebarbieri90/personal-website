---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
#description: " "
date: 
#repo: "#" # delete this line if you want blog-like posts for projects
#tags: []
weight: 1
draft: false
---

![](/resources/portfolio-img/)

<br>

#### **Authors**

[Barbieri Simone](http://barbierisimone.com/), Centre for Digital Entertainment, Bournemouth University, Thud Media

Ben Cawthorne, Thud Media

[Xiao Zhidong](https://staffprofiles.bournemouth.ac.uk/display/zxiao), Bournemouth University

[Yang Xiaosong](https://staffprofiles.bournemouth.ac.uk/display/xyang), Bournemouth University

<br>

#### **Abstract**



<br>

#### **Paper**

![](/resources/pdf-icon.png)[]() ( MB)

Published in []()

**DOI**: 

<br>

#### **Bibtex**

``` latex

```